<?php
session_start();

define('BASE_URL', 'http://' . $_SERVER['HTTP_HOST'] . '/eventbooking/');
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Event booking</title>
        <link href="<?php echo (BASE_URL); ?>css/bootstrap.min.css" rel="stylesheet" />
    </head>

    <body>