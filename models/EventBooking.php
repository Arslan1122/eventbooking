<?php

require_once 'DBTrait.php';

class EventBooking
{
    use DBTrait;

    public function getJsonData()
    {
        $jsonContent = file_get_contents('../eventbooking.json');
        $content = json_decode($jsonContent, true);

        for ($i = 0; $i < sizeof($content); $i++) {
            $this->saveJsonData($content[$i]);
        }
    }

    private function saveJsonData($data)
    {
        $objDB = $this->obj_db();
        $eventQuery = 'insert into events (event_name,event_date)'
                        .'Values'
                        .'("'.$data["event_name"]. '","'. $data["event_date"] . '")';
        $objDB->query($eventQuery);
        if($objDB->errno)
        {
            throw new Exception('Event Error ' .$objDB->error .'-' .$objDB->errno);
        }
        $event_id = $objDB->insert_id;
        $query = 'insert into participations (event_id,employee_name,employee_mail,participation_fee)'
                    .'Values'
                    .'("' . $event_id .'","'.$data['employee_name'].'","'.$data['employee_mail'].'","'.$data['participation_fee'].'")';
        $objDB->query($query);
        if($objDB->errno)
        {
            throw new Exception('Participation Error' .$objDB->error .'-' .$objDB->errno);
        }
    }

}