<?php require_once "./views/header.php"; ?>
<div class="container">
    <div class="row">
        <?php
            if(isset($_SESSION['success'])) {
                $successMsg = $_SESSION['success'];
                unset($_SESSION['success']);
            }
            if(isset($_SESSION['error'])) {
                $errorMsg = $_SESSION['error'];
                unset($_SESSION['error']);
            }
        ?>
        <?php if(isset($successMsg)) { ?>
            <div class="alert alert-success successAlert"><?php echo($successMsg); ?></div>
        <?php } ?>
        <?php if(isset($errorMsg)) { ?>
            <div class="alert alert-danger errorAlert"><?php echo($errorMsg); ?></div>
        <?php } ?>
        <form method="GET" action="<?php echo (BASE_URL); ?>process/process_json.php">
            <button class="btn btn-success">Save Json Data To Database</button>
        </form>
    </div>
</div>

<?php require_once "./views/footer.php" ?>