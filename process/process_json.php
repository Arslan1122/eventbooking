<?php
session_start();

require_once "../models/EventBooking.php";

$obj = new EventBooking();

try {
    $obj->getJsonData();
    $_SESSION['success'] = "Json data stored to database successfully !";
} catch (Exception $e) {
    $_SESSION['error'] = $e->getMessage();
}

header('location:../');